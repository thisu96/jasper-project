package com.ctechacademy.demo.repository;

import com.ctechacademy.demo.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author thisu96
 * @Date 21/06/2020
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long> {
}
