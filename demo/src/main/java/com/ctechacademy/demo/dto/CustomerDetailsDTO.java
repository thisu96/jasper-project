package com.ctechacademy.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * @author thisu96
 * @Date 06/08/2020
 * @Time 18:05
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CustomerDetailsDTO {
    private JRBeanCollectionDataSource beanCollectionDataSource;
}
