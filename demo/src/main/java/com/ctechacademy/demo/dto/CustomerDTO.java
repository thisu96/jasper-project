package com.ctechacademy.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * @author thisu96
 * @Date 21/06/2020
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CustomerDTO {
    private Long sequence;
    private String userName;
    private String name;
    private String address;
    private String contactNo;
    private String emailAddress;
}
