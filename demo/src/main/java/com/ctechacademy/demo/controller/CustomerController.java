package com.ctechacademy.demo.controller;

import com.ctechacademy.demo.dto.CustomerDTO;
import com.ctechacademy.demo.service.CustomerService;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;

/**
 * @author thisu96
 * @Date 21/06/2020
 */
@CrossOrigin
@RestController
@RequestMapping(path="/api")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping(value = "/customer/add")
    public ResponseEntity saveCustomer(@RequestBody CustomerDTO customerDTO) throws Exception {
        Long data = customerService.saveCustomer(customerDTO);
        if (data == null) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } else {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
    }

    @GetMapping(value = "/customer/{sequence}")
    public CustomerDTO getCustomer(@PathVariable Long sequence) throws Exception {
        CustomerDTO customer = customerService.getCustomer(sequence);
        if (customer == null) {
            ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return customer;
    }

    @GetMapping(value = "/customerList")
    public List<CustomerDTO> getAllCustomers() throws Exception {
        List<CustomerDTO> all = customerService.getAll();
       if (all == null || all.isEmpty()) {
           return Collections.emptyList();
        }
       return all;
    }

    @GetMapping(value = "/customer/{id}/print")
    public void generateCustomerDetails(HttpServletResponse response, @PathVariable Long id) throws Exception {
        JasperPrint jasperPrint = null;

        response.setContentType("application/x-download");
        response.setHeader("COntent-Disposition", String.format( "attachment; filename=\"customer-detail.pdf\""));

        OutputStream outputStream = response.getOutputStream();
        //service->data
        JasperPrint print = customerService.generateCustomerDetails(id);
        JasperExportManager.exportReportToPdfStream(print,outputStream);
    }

    @GetMapping(value = "/customerList/print")
    public void generateCustomerDetailsList(HttpServletResponse response) throws Exception {
        JasperPrint jasperPrint = null;

        response.setContentType("application/x-download");
        response.setHeader("COntent-Disposition", String.format( "attachment; filename=\"customer-detail-list.pdf\""));

        OutputStream outputStream = response.getOutputStream();
        //service->data
        jasperPrint = customerService.generateCustomerDetail();

        JasperExportManager.exportReportToPdfStream(jasperPrint,outputStream);
    }
}
