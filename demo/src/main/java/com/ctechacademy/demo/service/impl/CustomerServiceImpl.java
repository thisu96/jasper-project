package com.ctechacademy.demo.service.impl;

import com.ctechacademy.demo.dto.CustomerDTO;
import com.ctechacademy.demo.dto.CustomerDetailsDTO;
import com.ctechacademy.demo.entity.Customer;
import com.ctechacademy.demo.repository.CustomerRepository;
import com.ctechacademy.demo.service.CustomerService;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.*;

/**
 * @author thisu96
 * @Date 21/06/2020
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ResourceLoader resourceLoader;

    @Override
    public Long saveCustomer(CustomerDTO dto) throws Exception {
        Customer customer = new Customer();
        customer.setUserName(dto.getUserName());
        customer.setName(dto.getName());
        customer.setAddress(dto.getAddress());
        customer.setContactNo(dto.getContactNo());
        customer.setEmailAddress(dto.getEmailAddress());
        return customerRepository.save(customer).getSequence();
    }

    @Override
    public CustomerDTO getCustomer(Long sequence) throws Exception {
        Customer customer = customerRepository.findById(sequence).get();
        CustomerDTO dto = new CustomerDTO();
        dto.setSequence(customer.getSequence());
        dto.setUserName(customer.getUserName());
        dto.setName(customer.getName());
        dto.setAddress(customer.getAddress());
        dto.setContactNo(customer.getContactNo());
        dto.setEmailAddress(customer.getEmailAddress());
        return dto;
    }

    @Override
    public List<CustomerDTO> getAll() throws Exception {
        List<Customer> all = customerRepository.findAll();
        List<CustomerDTO> targetCustomerList = new ArrayList<>();
        for (Customer c :all) {
            CustomerDTO dto = new CustomerDTO();
            dto.setSequence(c.getSequence());
            dto.setName(c.getName());
            dto.setUserName(c.getUserName());
            dto.setAddress(c.getAddress());
            dto.setContactNo(c.getContactNo());
            dto.setEmailAddress(c.getEmailAddress());
            targetCustomerList.add(dto);
        }
        return targetCustomerList;
    }

    @Override
    public JasperPrint generateCustomerDetails(Long sequence) throws Exception {
        CustomerDTO customer = getCustomer(sequence);

        InputStream inputStream = resourceLoader.getResource("classpath:customer.jasper").getInputStream();

        Map<String, Object> parameters = new HashMap<String, Object>();

        JRBeanArrayDataSource beanArrayDataSource = new JRBeanArrayDataSource(new CustomerDTO[]{customer});
        return JasperFillManager.fillReport(inputStream,parameters,beanArrayDataSource);
    }

    @Override
    public JasperPrint generateCustomerDetail() throws Exception {
        CustomerDetailsDTO dto = generateCustomerDetailsList();
        InputStream inputStream = resourceLoader.getResource("classpath:customer.jasper").getInputStream();

        Map<String, Object> parameters = new HashMap<String, Object>();

        JRBeanArrayDataSource beanArrayDataSource = new JRBeanArrayDataSource(new CustomerDetailsDTO[]{dto});
        return JasperFillManager.fillReport(inputStream,parameters,beanArrayDataSource);
    }

    @Override
    public CustomerDetailsDTO generateCustomerDetailsList() throws Exception {
        CustomerDetailsDTO customerDetails = new CustomerDetailsDTO();
        customerDetails.setBeanCollectionDataSource(generateCustomerDetails());
        return customerDetails;
    }

    private JRBeanCollectionDataSource generateCustomerDetails() throws Exception {
        List<CustomerDTO> customerDTOList = getAll();
        return new JRBeanCollectionDataSource(customerDTOList, false);
    }

    private void removeBlankPage(List<JRPrintPage> pages) {
        for (Iterator<JRPrintPage> i = pages.iterator(); i.hasNext(); ) {
            JRPrintPage page = i.next();
            if (page.getElements().isEmpty())
                i.remove();
        }
    }
}
