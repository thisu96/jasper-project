package com.ctechacademy.demo.service;

import com.ctechacademy.demo.dto.CustomerDTO;
import com.ctechacademy.demo.dto.CustomerDetailsDTO;
import net.sf.jasperreports.engine.JasperPrint;

import java.util.List;

/**
 * @author thisu96
 * @Date 21/06/2020
 */
public interface CustomerService {

    Long saveCustomer(CustomerDTO dto) throws Exception;
    CustomerDTO getCustomer(Long sequence) throws Exception;
    List<CustomerDTO> getAll() throws Exception;
    JasperPrint generateCustomerDetails(Long sequence) throws Exception;
    JasperPrint generateCustomerDetail() throws Exception;
    CustomerDetailsDTO generateCustomerDetailsList() throws Exception;
}
