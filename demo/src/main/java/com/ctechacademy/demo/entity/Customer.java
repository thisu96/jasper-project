package com.ctechacademy.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author thisu96
 * @Date 30/07/2020
 */
@Entity
@Table(name = "CUSTOMER")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Customer {
    @Id
    @Column(name = "SEQUENCE", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long sequence;
    @Column(name = "USER_NAME")
    private String userName;
    @Column(name = "CUSTOMER_NAME")
    private String name;
    @Column(name = "ADDRESS")
    private String address;
    @Column(name = "EMAIL_ADDRESS")
    private String emailAddress;
    @Column(name = "CONTACT_NO")
    private String contactNo;
}
